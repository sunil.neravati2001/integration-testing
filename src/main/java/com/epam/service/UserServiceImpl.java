package com.epam.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.dto.UserDTO;
import com.epam.entity.User;
import com.epam.exception.UserException;
import com.epam.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final ModelMapper modelMapper;

	private final UserRepository userRepository;

	@Override
	public UserDTO addUser(UserDTO userDTO) {

		return modelMapper.map(userRepository.save(modelMapper.map(userDTO, User.class)), UserDTO.class);
	}

	@Override
	public UserDTO getUser(int userId) {

		User user = userRepository.findById(userId)
				.orElseThrow(() -> new UserException("User Not found with the given Id"));
		return modelMapper.map(user, UserDTO.class);
	}
	

}

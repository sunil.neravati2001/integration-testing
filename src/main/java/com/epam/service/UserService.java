package com.epam.service;

import com.epam.dto.UserDTO;

public interface UserService {

	public UserDTO addUser(UserDTO userDTO);
	public UserDTO getUser(int userId);
	
}

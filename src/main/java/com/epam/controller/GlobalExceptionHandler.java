package com.epam.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.dto.ExceptionResponse;
import com.epam.exception.UserException;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(UserException.class)
	public ResponseEntity<ExceptionResponse> userNotFoundExceptionalHandler(UserException message, WebRequest request) {
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				message.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

}

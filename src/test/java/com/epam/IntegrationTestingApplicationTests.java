package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.epam.dto.UserDTO;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
class IntegrationTestingApplicationTests {

	@LocalServerPort
	private int port;

	private String baseUrl = "http://localhost:";

	private static RestTemplate restTemplate;

	@BeforeAll
	public static void init() {

		restTemplate = new RestTemplate();
	}

	@BeforeEach
	public void setUp() {
		baseUrl = baseUrl.concat(port + "").concat("/users");
	}

	@Test
	void testAddUser() {
		UserDTO userDTO = new UserDTO();
		userDTO.setUserName("new user");
		userDTO.setMobileNo("1234");
		userDTO.setLocation("Hyderabad");

		UserDTO response = restTemplate.postForObject(baseUrl, userDTO, UserDTO.class);

		assertEquals("new user", response.getUserName());
		assertEquals("1234", response.getMobileNo());
	}

	@Test
	@Sql(statements = "INSERT INTO user(id,user_name, mobile_no, location) VALUES (100,'dummy user', '1234', 'hyd')", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM user WHERE id=100", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	void getUser() {

		UserDTO response = restTemplate.getForObject(baseUrl.concat("/").concat(100+ ""), UserDTO.class);

		assertEquals("dummy user", response.getUserName());

	}
	
	@Test
	void getUserWithInvalidId() {
		
		assertThrows(HttpClientErrorException.class,()->restTemplate.getForObject(baseUrl.concat("/").concat(100+ ""), UserDTO.class));
	}

}

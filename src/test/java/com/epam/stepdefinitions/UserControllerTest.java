package com.epam.stepdefinitions;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.epam.dto.UserDTO;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@RunWith(SpringRunner.class)
@CucumberContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class UserControllerTest {

	private UserDTO userDTO;
	private ResponseEntity<UserDTO> responseEntity;

	@LocalServerPort
	private int port;

	private String baseUrl = "http://localhost:";

	private RestTemplate restTemplate = new RestTemplate();

	private int id;

	@Given("a user with name {string}, mobile number {string}, and location {string}")
	public void a_user_with_name_mobile_number_and_location(String username, String mobilenumber, String location) {
		baseUrl = baseUrl.concat(port + "").concat("/users");
		userDTO = new UserDTO(username, mobilenumber, location);
	}

	@When("I request to add the user")
	public void i_request_to_add_the_user() {
		responseEntity = restTemplate.postForEntity(baseUrl, userDTO, UserDTO.class);
	}

	@Then("the response status code should be {int}")
	public void the_response_status_code_should_be(int expectedStatusCode) {
		HttpStatusCode statusCode = responseEntity.getStatusCode();
		Assert.assertEquals(expectedStatusCode, statusCode.value());
	}

	@Then("the user's name in the response should be {string}")
	public void the_user_s_name_in_the_response_should_be(String expectedName) {
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			UserDTO userDTO = responseEntity.getBody();
			Assert.assertEquals(expectedName, userDTO.getUserName().trim());
		}
	}

	@Then("the user's mobile number in the response should be {string}")
	public void the_user_s_mobile_number_in_the_response_should_be(String expectedMobileNo) {
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			UserDTO userDTO = responseEntity.getBody();
			Assert.assertEquals(expectedMobileNo, userDTO.getMobileNo().trim());
		}
	}

	@Then("the user's location in the response should be {string}")
	public void the_user_s_location_in_the_response_should_be(String expectedLocation) {
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			UserDTO userDTO = responseEntity.getBody();
			Assert.assertEquals(expectedLocation, userDTO.getLocation().trim());
		}
	}

	@Given("an existing user with ID {int}")
	public void an_existing_user_with_id(int userId) {
		baseUrl = baseUrl.concat(port + "").concat("/users");
		id = userId;
	}

	@When("I request to get the user by ID")
	public void i_request_to_get_the_user_by_id() {
		try {
		responseEntity = restTemplate.getForEntity(baseUrl.concat("/").concat(id + ""), UserDTO.class);
		}catch (HttpClientErrorException ex) {
	        responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }

	}

	@Given("a non-existing user with ID {int}")
	public void a_non_existing_user_with_id(int userId) {
		baseUrl = baseUrl.concat(port + "").concat("/users");
		id = userId;
	}

}
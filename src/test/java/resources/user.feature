Feature: User Management
  As a system user
  I want to manage user information
  So that I can add and retrieve user details

  Scenario: Adding a new user
    Given a user with name "Eve", mobile number "5555555555", and location "Garden"
    When I request to add the user
    Then the response status code should be 200
    And the user's name in the response should be "Eve"
    And the user's mobile number in the response should be "5555555555"
    And the user's location in the response should be "Garden"

  Scenario: Retrieving an existing user by ID
    Given an existing user with ID 1
    When I request to get the user by ID
    Then the response status code should be 200
    And the user's name in the response should be "Alice"
    And the user's mobile number in the response should be "1234567890"
    And the user's location in the response should be "Wonderland"

  Scenario: Retrieving a non-existing user by ID
    Given a non-existing user with ID 999
    When I request to get the user by ID
    Then the response status code should be 404

